export interface RecipePuppyRecipe {
    title: string,
    href: string,
    ingredients: string,
    thumbnail: string
}

export interface RecipePuppyResponse {
    title: string;
    version: number;
    href: string;
    results: RecipePuppyRecipe[]
}
