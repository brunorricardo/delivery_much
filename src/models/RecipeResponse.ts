import Recipe from "./recipe";

export default interface RecipeResponse {
    keywords: string[];
    message?: string;
    recipes: Recipe[];
}
