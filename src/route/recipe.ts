import express from 'express';
import RecipeResponse from "../models/RecipeResponse";
import {RecipeController} from "../controller/RecipeController";

const router = express.Router();

router.get('', async (req, res): Promise<void> => {

    let resposta!: RecipeResponse;

    const urlQuery: { i?: string } = req.query;

    const {usedIngredients, ignoredIngredients} = RecipeController.organizeParams(urlQuery.i);

    try {
        const recipes = await RecipeController.findRecipe(usedIngredients);

        resposta = {
            recipes: recipes,
            keywords: usedIngredients,
            message: receivedIngredients.length > 3 ? `You passed more than 3 ingredients, therefore [${ignoredIngredients.join(', ')}] ${ignoredIngredients.length > 1 ? 'are' : 'is'} not used on this search` : ''
        };
        res.json(resposta);
    }catch (err) {
        res.status(500).json({
           error: "Algum dos serviços terceiros estão fora do ar, tente novamente mais tarde"
        });
    }

});

export default router;
