import express from 'express';
import dotenv from 'dotenv';
import recipe from "./route/recipe";

const app = express();
const port = 8080;

dotenv.config();

app.use('/recipe', recipe);

app.listen(port, () => {
   console.log(`Rest API running at host:port :${port}`);
});
