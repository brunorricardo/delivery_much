import {RecipeController} from "../controller/RecipeController";

it('Processamento dos ingredientes deve retornar objeto', function () {
    const ingredientesProcessados = RecipeController.organizeParams("tomato,onion,letuce,garlic,chilli,salt,oil");
    expect(typeof ingredientesProcessados).toBe('object');
});

it('Processamento dos ingredientes deve retornar um array', function () {
    const ingredientesProcessados = RecipeController.organizeParams("tomato,onion,letuce,garlic,chilli,salt,oil");
    expect(Array.isArray(ingredientesProcessados.usedIngredients)).toBeTruthy();
});

it('Processamento dos ingredientes deve retornar um array mesmo sem argumentos', function () {
    const ingredientesProcessados = RecipeController.organizeParams("");
    expect(Array.isArray(ingredientesProcessados.usedIngredients)).toBeTruthy();
});

it('Processamento dos ingredientes deve retornar um array dos items sobresalientes', function () {
    const ingredientesProcessados = RecipeController.organizeParams("tomato,onion,letuce,garlic,chilli,salt,oil");
    expect(Array.isArray(ingredientesProcessados.ignoredIngredients)).toBeTruthy();
});

it('A Busca de receitas deve retornar um array', async function () {
    const receitasEncontradas = await RecipeController.findRecipe(["oil", "vinegar", "letuce"]);
    expect(Array.isArray(receitasEncontradas)).toBeTruthy();
});

it('Lançar erro caso a busca não tenha ao menos um ingrediente', async function () {
    // const receitasEncontradas = await RecipeController.findRecipe([]);
    await expect(RecipeController.findRecipe([])).rejects.toThrow('The search must have at least one ingredient');
});

it('A Busca de receitas deve retornar um array', async function () {
    const receitasEncontradas = await RecipeController.findRecipe(["oil", "vinegar", "letuce"]);
    expect(Array.isArray(receitasEncontradas)).toBeTruthy();
});

it('A Busca de receitas deve retornar array com title, ingredients, link e gif', async function () {
    const receitasEncontradas = await RecipeController.findRecipe(["oil", "vinegar", "letuce"]);

    expect(receitasEncontradas).toEqual(
        expect.arrayContaining([
            expect.objectContaining({
                title: expect.any(String),
                ingredients: expect.any(Array),
                link: expect.any(String),
                gif: expect.any(String),
            })
        ])
    )
});
