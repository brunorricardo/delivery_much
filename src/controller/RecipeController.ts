import axios from 'axios';
import {RecipePuppyRecipe} from "../models/RecipePuppyResponse";
import Recipe from "../models/recipe";
import GiphyController from "./GiphyController";

export class RecipeController {

    static organizeParams(params: string): { usedIngredients: string[], ignoredIngredients: string[] } {
        const receivedIngredients: string[] = params.split(',').sort();

        const usedIngredients = receivedIngredients.slice(0, 3);
        const ignoredIngredients = receivedIngredients.filter((q, i) => i >= 3);

        return {
            usedIngredients,
            ignoredIngredients
        }
    }

    static async findRecipe(ingredients: string[]): Promise<Recipe[]> {
        if (ingredients.length < 1) {
            throw new Error('The search must have at least one ingredient');
        }

        const url = `${process.env.RECIPE_API_ENDPOINT}?i=${ingredients.join(',')}`;
        const recipesFound = await axios.get(url);
        return await RecipeController.conformRecipes(recipesFound.data.results);
    }

    static async conformRecipes(recipePuppyRecipe: RecipePuppyRecipe[]): Promise<Recipe[]> {
        return await Promise.all(recipePuppyRecipe.map(async (recipe) => {
            return {
                title: recipe.title,
                link: recipe.href,
                ingredients: recipe.ingredients.split(','),
                gif: await GiphyController.findGif(recipe.title)
            }
        }));
    }



}
