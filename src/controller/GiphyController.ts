import axios from 'axios';
import GiphyResponse from "../models/GiphyResponse";

export default class GiphyController {
    static async findGif(search: string): Promise<string> {
        const url = `${process.env.GIPHY_API_ENDPOINT}api_key=${process.env.GIPHY_API_KEY}&q=${encodeURIComponent(search)}&limit=1&offset=0&rating=g&lang=en`;
        const gifSearch = await axios.get<GiphyResponse>(url);
        return gifSearch.data.data[0].embed_url;
    }
}
