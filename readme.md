# Bruno Ricardo Delivery Much Tech Challenge

Para rodar a API Rest, faça os seguintes passos:

1. Clone este repositório
2. Já na raiz do ṕrojeto, execute o seguinte comando ``docker build .``
3. Depois do processo de build da imagem concluido, o endpoint para consulta das receitas estará concluido;
4. Caso queria execultar os testes, rode o comando ``npm run test``

Para execultar o projeto, usei o Typescript, acho mais oeganizado e "error-free" usando a tipagem dos objetos :D
